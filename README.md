# Chomsky

Shall be used to parse unambigous context-free-grammars. 

Is able to detect left recursion and infinite loops.

Needs no lexing before.

## Requirements
The library is meant to be PHP7 compatible. Some work needs to be done to make it work properly using scalar type
hints, this will happen step by step...

## Methodology
Chomsky is a parser-combinator and acts like a LR-Parser