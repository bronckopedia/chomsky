<?php
/**
 * Created by PhpStorm.
 * User: heinemann
 * Date: 12.02.16
 * Time: 08:40
 */

namespace chomsky\tests\unit;

require_once 'Text/Template/Autoload.php';
require_once 'PHPUnit/Autoload.php';

$srcDir = $srcDir = implode(DIRECTORY_SEPARATOR, array(dirname(__FILE__), '..', '..', 'src'));

spl_autoload_register(function($class) use ($srcDir) {
	$parts = explode('\\', $class);
	array_shift($parts);

	$filepath = $srcDir . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $parts) . '.php';

	if(preg_match("/chomsky/", $class) === 1) {
		@require_once $filepath;
	}
});