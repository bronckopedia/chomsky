<?php
/**
 * Created by PhpStorm.
 * User: heinemann
 * Date: 18.02.16
 * Time: 08:20
 */

namespace chomsky\tests\unit\Parsers;

use chomsky\Parsers\EmptyParser;
use chomsky\Exceptions;

class EmptyParserTest extends \PHPUnit_Framework_TestCase
{
	public function testEmptyParser()
	{
		$emptyParser = new EmptyParser(
			function($result) {
				var_dump($result); die();
			}
		);

		$emptyParser->parse("Hallo Welt");

		$this->assertFalse(true);
	}

	public function testDefaultCallback()
	{
		$emptyParser = new EmptyParser();
		$this->assertNull($emptyParser->defaultCallback());
	}

	public function testGetResult()
	{
		$emptyParser = new EmptyParser();

		$i = 5;
		$this->assertEquals(array('j' => $i, 'args' => array()), $emptyParser->getResult('bla', $i));
	}

	public function testEvaluateNullability()
	{
		$emptyParser = new EmptyParser();

		$this->assertTrue($emptyParser->evaluateNullability());
	}
}
