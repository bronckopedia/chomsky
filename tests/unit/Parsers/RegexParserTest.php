<?php
/**
 * Created by PhpStorm.
 * User: heinemann
 * Date: 12.02.16
 * Time: 08:25
 */

namespace chomsky\tests\unit\Parsers;

use chomsky\Parsers\RegexParser;
use chomsky\Exceptions;


class RegexParserTest extends \PHPUnit_Framework_TestCase
{
	public function regexProvider()
	{
		return array(
			array('/^[a-z]*/', 'blablubb', 'blablubb'),
			array('/^[0-9]*/', 'blablubb', false)
		);
	}

	/**
	 * @dataProvider regexProvider
	 */
	public function testMatches($pattern, $string, $expected)
	{
		$regexParser = new RegexParser($pattern);

		$this->assertEquals($expected, $regexParser->parse($string));
	}

	public function testMatchWithCallback()
	{
		$regexParser = new RegexParser("/^[a-z]*$/", function($match) {
			return strrev($match);
		});

		$string = "blablubb";

		$this->assertEquals(strrev($string), $regexParser->parse($string));
	}

	/**
	 * @expectedException chomsky\Exceptions\GrammarException
	 */
	public function testNotCompleteString()
	{
		$regexParser = new RegexParser('/^[a-z]*/');

		$expected = 'blablubb';

		$this->assertEquals($expected, $regexParser->parse($expected));
	}
}
