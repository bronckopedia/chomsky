<?php

/**
 * Created by PhpStorm.
 * User: heinemann
 * Date: 30.01.16
 * Time: 18:24
 */
namespace chomsky\Parsers;

class GreedyParser extends LoopParser {

	public function __construct($internal, callable $callback = null) {
		$this->string = sprintf("new %s(%s)", get_class(), $internal);

		parent::__construct($internal, 0, null, $callback);
	}
}