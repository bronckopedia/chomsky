<?php

/**
 * Created by PhpStorm.
 * User: heinemann
 * Date: 30.01.16
 * Time: 18:11
 */
namespace chomsky\Parsers;

use chomsky\Exceptions;

class StringParser extends StaticParser {

	private $needle;

	public function __construct($needle, $callback = null) {
		if(!is_string($needle)) {
			throw new \UnexpectedValueException("StringParser needs string to parse ;-)");
		}

		$this->needle = $needle;
		$this->string = "new ".get_class()."(".var_export($needle, true).")";

		parent::__construct($callback);
	}

	public function defaultCallback() {
		return func_get_arg(0);
	}

	public function getResult(string $string, int $i = 0) : array {
		if(strpos($string, $this->needle, $i) === $i) {
			return array(
				"j" => $i + strlen($this->needle),
				"args" => array($this->needle)
			);
		}

		throw new Exceptions\ParseFailureException($this." could not find string ".var_export($this->needle, true), $i, $string);
	}

	public function evaluateNullability() : bool {
		return $this->needle === "";
	}

}