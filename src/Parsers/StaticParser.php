<?php
namespace chomsky\Parsers;

/**
 * Created by PhpStorm.
 * User: heinemann
 * Date: 30.01.16
 * Time: 18:07
 */

abstract class StaticParser extends BasicParser {

	public function __construct($callback)
	{
		parent::__construct(array(), $callback);
	}

	/**
	 * Get Startword
	 *
	 * @return array
	 */
	public function firstSet()
	{
		return array();
	}
}