<?php
namespace chomsky\Parsers;

/**
 * Created by PhpStorm.
 * User: heinemann
 * Date: 30.01.16
 * Time: 17:55
 */

use chomsky\Exceptions;

abstract class BasicParser
{
	/** @var BasicParser[] */
	protected $parsers = array();
	protected $callback;
	protected $string = "";
	protected $nullable = false;

	public function __construct(array $parsers, callable $callback = null)
	{
		if(!is_string($this->string)) {
			throw new \UnexpectedValueException("no valid executable string populated");
		}

		foreach($parsers as $parser) {
			if(!is_string($parser) and !($parser instanceof \chomsky\Parsers\BasicParser)) {
				throw new \UnexpectedValueException(var_export($parser, true) . ' shall be a string or of type BasicParser');
			}
		}

		$this->parsers = $parsers;
		$this->callback = $callback ?: array($this, 'defaultCallback');
	}

	public function __toString() : string
	{
		return $this->string;
	}

	/**
	 * Get internal parsers
	 *
	 * @param string|null $name Optional name
	 * @return BasicParser|BasicParser[]|string
	 */
	public function getParsers(string $name = null)
	{
		if(!is_null($name)) {
			if (array_key_exists($name, $this->parsers)) {
				return $this->parsers[$name];
			} else {
				throw new \UnexpectedValueException("parser with name " . $name . " is not registered");
			}
		}

		return $this->parsers;
	}

	/**
	 * Set parser
	 *
	 * @param string $name
	 * @param BasicParser $parser
	 */
	public function setParser(string $name, BasicParser $parser)
	{
		$this->parsers[$name] = $parser;
	}

	abstract public function getResult(string $string, int $i = 0) : array;

	abstract public function evaluateNullability() : bool;

	abstract public function firstSet();

	abstract public function defaultCallback();

	/**
	 * Check if parser matches
	 *
	 * @param mixed $string
	 * @param int $i
	 * @return array
	 */
	public function match($string, $i = 0) : array {
		$result = $this->getResult($string, $i);

		return array(
			"j" => $result["j"],
			"value" => call_user_func_array($this->callback, $result["args"])
		);
	}

	/**
	 * Parse string
	 *
	 * @param string $string
	 * @return mixed
	 * @throws Exceptions\ParseFailureException
	 */
	public function parse(string $string) {
		$result = $this->getResult($string, 0);

		if($result["j"] != strlen($string)) {
			throw new Exceptions\ParseFailureException("Parsing completed", $result["j"], $string);
		}

		return call_user_func_array($this->callback, $result["args"]);
	}

	/**
	 * Serialize array to readable string
	 *
	 * @param array $array
	 * @return string
	 */
	protected static function serialiseArray(array $array) : string {
		$string = "array(";

		foreach(array_keys($array) as $keyId => $key) {
			$string .= var_export($key, true)." => ";

			if(is_string($array[$key])) {
				$string .= var_export($array[$key], true);
			} else {
				$string .= $array[$key]->__toString();
			}

			if($keyId + 1 !== count($array)) {
				$string .= ", ";
			}
		}

		$string .= ")";

		return $string;
	}
}