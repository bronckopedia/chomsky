<?php

/**
 * Created by PhpStorm.
 * User: heinemann
 * Date: 30.01.16
 * Time: 18:27
 */
namespace chomsky\Parsers;

class ConcatParser extends BasicParser {

	public function __construct($parsers, $callback = null) {

		$this->string = sprintf("new %s(%s)", get_class(), self::serialiseArray($parsers));

		parent::__construct($parsers, $callback);
	}

	public function defaultCallback() {
		return func_get_args();
	}

	public function getResult(string $string, int $i = 0) : array {
		$j = $i;
		$args = array();
		foreach($this->getParsers() as $parser) {
			$match = $parser->match($string, $j);
			$j = $match["j"];
			$args[] = $match["value"];
		}
		return array("j" => $j, "args" => $args);
	}

	function firstSet() {
		$firstSet = array();
		foreach($this->getParsers() as $internal) {
			$firstSet[] = $internal;

			if(!$internal->nullable) {
				break;
			}
		}
		return $firstSet;
	}

	public function evaluateNullability() : bool {
		foreach($this->getParsers() as $internal) {
			if(!$internal->nullable) {
				return false;
			}
		}
		return true;
	}
}