<?php
/**
 * Created by PhpStorm.
 * User: heinemann
 * Date: 30.01.16
 * Time: 18:20
 */
namespace chomsky\Parsers;

use chomsky\Exceptions;

class LoopParser extends BasicParser {

	private $lower;

	public $optional;

	public function __construct($internal, $lower, $upper, $callback = null) {
		$this->lower = $lower;

		if(is_null($upper)) {
			$this->optional = null;
		} else {
			if($upper < $lower) {
				throw new \OutOfBoundsException("upper can not be lower than lower, think about it!");
			}

			$this->optional = $upper - $lower;
		}

		$this->string = "new ".get_class()."(".$internal.", ".var_export($lower, true).", ".var_export($upper, true).")";

		parent::__construct(array($internal), $callback);
	}

	public function defaultCallback()
	{
		return func_get_args();
	}

	public function getResult(string $string, int $i = 0) : array
	{
		$result = array("j" => $i, "args" => array());

		for($k = 0; $k < $this->lower; $k++) {
			$match = $this->parsers[0]->match($string, $result["j"]);
			$result["j"] = $match["j"];
			$result["args"][] = $match["value"];
		}

		for($k = 0; $this->optional === null || $k < $this->optional; $k++) {
			try {
				$match = $this->parsers[0]->match($string, $result["j"]);
				$result["j"] = $match["j"];
				$result["args"][] = $match["value"];
			} catch(Exceptions\ParseFailureException $e) {
				break;
			}
		}

		return $result;
	}

	public function evaluateNullability() : bool {
		return ($this->lower == 0 || $this->parsers[0]->nullable === true);
	}

	public function firstSet() {
		return array($this->parsers[0]);
	}

	public function isOptional()
	{
		return $this->optional;
	}
}