<?php
/**
 * Created by PhpStorm.
 * User: heinemann
 * Date: 30.01.16
 * Time: 18:16
 */
namespace chomsky\Parsers;

use chomsky\Exceptions;

class AlternativeParser extends BasicParser {

	public function __construct($parsers, $callback = null) {
		if(count($parsers) === 0) {
			throw new \UnexpectedValueException("need at least one parser\n");
		}

		$this->parsers = $parsers;
		$this->string = "new ".get_class()."(".$this->serialiseArray($parsers).")";

		parent::__construct($parsers, $callback);
	}

	public function defaultCallback() {
		return func_get_arg(0);
	}

	public function getResult(string $string, int $i = 0) : array {
		foreach($this->getParsers() as $internal) {
			try {
				$match = $internal->match($string, $i);
			} catch(Exceptions\ParseFailureException $e) {
				continue;
			}
			return array(
				"j" => $match["j"],
				"args" => array($match["value"])
			);
		}

		throw new Exceptions\ParseFailureException($this." could not match another token", $i, $string);
	}

	public function evaluateNullability() : bool {
		foreach($this->getParsers() as $internal) {
			if($internal->nullable) {
				return true;
			}
		}
		return false;
	}

	public function firstSet() {
		return $this->getParsers();
	}
}