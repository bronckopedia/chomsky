<?php

/**
 * Created by PhpStorm.
 * User: heinemann
 * Date: 30.01.16
 * Time: 18:08
 */
namespace chomsky\Parsers;

class EmptyParser extends StaticParser {
	public function __construct($callback = null) {
		$this->string = "new ".get_class()."()";
		parent::__construct($callback);
	}

	public function defaultCallback() {
		return null;
	}

	public function getResult(string $string, int $i = 0) : array {
		return array(
			"j" => $i,
			"args" => array()
		);
	}

	public function evaluateNullability() : bool {
		return true;
	}
}