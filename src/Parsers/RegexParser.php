<?php
namespace chomsky\Parsers;

/**
 * Created by PhpStorm.
 * User: heinemann
 * Date: 30.01.16
 * Time: 18:14
 */

use chomsky\Exceptions;

class RegexParser extends StaticParser {

	private $pattern;

	public function __construct(string $pattern, $callback = null) {

		$this->string = "new ".get_class()."(".var_export($pattern, true).")";

		if(substr($pattern, 1, 1) !== "^") {
			throw new Exceptions\GrammarException($this." does not starts at the beginning of the string!");
		}

		$this->pattern = $pattern;

		parent::__construct($callback);
	}

	public function defaultCallback() {
		return func_get_arg(0);
	}

	public function getResult(string $string, int $i = 0) : array {
		if(preg_match($this->pattern, substr($string, $i), $matches) === 1) {
			return array(
				"j" => $i + strlen($matches[0]),
				"args" => $matches
			);
		}

		throw new Exceptions\ParseFailureException($this." could not match expression ".var_export($this->pattern, true), $i, $string);
	}

	public function evaluateNullability() : bool {
		return (preg_match($this->pattern, "", $matches) === 1);
	}

}