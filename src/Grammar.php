<?php
namespace chomsky;

use chomsky\Exceptions\GrammarException;
use chomsky\Parsers;

/**
 * Created by PhpStorm.
 * User: heinemann
 * Date: 30.01.16
 * Time: 18:31
 */

class Grammar extends Parsers\BasicParser
{
	/** @var string $s Startword */
	private $S;

	/**
	 * Create grammar instance
	 *
	 * @param string $S Startword
	 * @param Parsers\BasicParser[] $parsers Set of parsers
	 * @param callable|null $callback Callback
	 *
	 * @throws GrammarException
	 */
	public function __construct(string $S, array $parsers, callable $callback = null)
	{
		$this->string = "new ".get_class()."(".var_export($S, true).", ".$this->serialiseArray($parsers).")";

		$this->callback = function() { return func_get_arg(0); };

		parent::__construct($parsers, $callback);

		if(!array_key_exists($S, $this->parsers)) {
			throw new GrammarException("This grammar begins with rule '".var_export($S, true)."' but no parser with this name was given.");
		}

		$this->S = $S;

		$this->resolve($this);

		while(1) {
			foreach($this->parsers as $parser) {
				if($parser->nullable === true) {
					continue;
				}

				if(!$parser->evaluateNullability()) {
					continue;
				}

				$parser->nullable = true;
				continue(2);
			}

			break;
		}

		foreach($this->parsers as $parser) {

			$firstSet = array($parser);
			$i = 0;
			while($i < count($firstSet)) {
				$current = $firstSet[$i];
				foreach($current->firstSet() as $next) {

					if($next === $parser) {
						throw new GrammarException("This grammar is left-recursive in ".$parser.".");
					}

					for($j = 0; $j < count($firstSet); $j++) {
						if($next === $firstSet[$j]) {
							continue(2);
						}
					}

					$firstSet[] = $next;
				}
				$i++;
			}
		}

		foreach($this->parsers as $parser) {
			if(!($parser instanceof Parsers\LoopParser)) {
				continue;
			}
			if($parser->optional !== null) {
				continue;
			}

			$parsers = $parser->getParsers();

			if(reset($parsers)->nullable) {
				throw new GrammarException($parser." has internal parser ".reset($parser->getParsers()).", which matches the empty string. This will cause infinite loops when parsing.");
			}
		}
	}

	private function resolve($parser)
	{
		$keys = array_keys($parser->getParsers());
		for($i = 0; $i < count($keys); $i++) {
			$key = $keys[$i];

			if(is_string($parser->getParsers($key))) {

				$name = $parser->getParsers($key);
				if(!array_key_exists($name, $this->parsers)) {
					throw new GrammarException($parser." contains a reference to another parser ".var_export($name, true)." which cannot be found");
				}

				$parser->setParser($key, $this->parsers[$name]);
			}	else {
				$parser->setParser($key, $this->resolve($parser->getParsers($key)));
			}
		}

		return $parser;
	}

	public function getResult(string $string, int $i = 0) : array
	{
		$match = $this->getParsers($this->S)->match($string, $i);
		return array(
			"j" => $match["j"],
			"args" => array($match["value"])
		);
	}

	/**
	 * Evaluate nullability
	 *
	 * @return bool
	 */
	public function evaluateNullability() : bool
	{
		return ($this->getParsers($this->S)->nullable === true);
	}

	/**
	 * Returns startword
	 *
	 * @return array
	 */
	public function firstSet()
	{
		return array($this->internals[$this->S]);
	}

	/**
	 * Defines default callback
	 *
	 * @return mixed
	 */
	public function defaultCallback()
	{
		return func_get_arg(0);
	}
}