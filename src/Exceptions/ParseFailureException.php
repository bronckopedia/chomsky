<?php
/**
 * Created by PhpStorm.
 * User: heinemann
 * Date: 11.02.16
 * Time: 19:36
 */
namespace chomsky\Exceptions;

class ParseFailureException extends \Exception
{
	public function __construct($message, $i, $string, $code = 0, \Exception $previous = null) {
		$message .= " at position ".var_export($i, true)." in string ". $string;
		parent::__construct($message, $code);
	}
}