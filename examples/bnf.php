<?php
/**
 * Created by PhpStorm.
 * User: heinemann
 * Date: 19.02.16
 * Time: 08:22
 */
namespace chomsky;

require_once 'autoload.php';

use chomsky\Parsers;

$bnfGrammar = new Grammar(
	"<syntax>",
	array(
		"<syntax>" => new Parsers\ConcatParser(
			array(
				"<rules>",
				"OPT-WHITESPACE"
			),
			function($rules, $whitespace) { return $rules; }
		),

		"<rules>" => new Parsers\LoopParser(
			"<ruleoremptyline>",
			1,
			null,
			function() {
				$rules = array();
				foreach(func_get_args() as $rule) {

					// blank line
					if($rule === null) {
						continue;
					}

					$rules[] = $rule;
				}
				return $rules;
			}
		),

		"<ruleoremptyline>" => new Parsers\AlternativeParser(
			array("<rule>", "<emptyline>")
		),

		"<emptyline>" => new Parsers\ConcatParser(
			array("OPT-WHITESPACE", "EOL"),
			function($whitespace, $eol) {
				return null;
			}
		),

		"<rule>" => new Parsers\ConcatParser(
			array(
				"OPT-WHITESPACE",
				"RULE-NAME",
				"OPT-WHITESPACE",
				new Parsers\StringParser("::="),
				"OPT-WHITESPACE",
				"<expression>",
				"EOL"
			),
			function(
				$whitespace1,
				$rule_name,
				$whitespace2,
				$equals,
				$whitespace3,
				$expression,
				$eol
			) {
				return array(
					"rule-name"  => $rule_name,
					"expression" => $expression
				);
			}
		),

		"<expression>" => new Parsers\ConcatParser(
			array(
				"<list>",
				"<pipelists>"
			),
			function($list, $pipelists) {
				array_unshift($pipelists, $list);
				return new Parsers\AlternativeParser($pipelists);
			}
		),

		"<pipelists>" => new Parsers\GreedyParser("<pipelist>"),

		"<pipelist>" => new Parsers\ConcatParser(
			array(
				new Parsers\StringParser("|"),
				"OPT-WHITESPACE",
				"<list>"
			),
			function($pipe, $whitespace, $list) {
				return $list;
			}
		),

		"<list>" => new Parsers\LoopParser(
			"<term>",
			1,
			null,
			function() {
				return new Parsers\ConcatParser(func_get_args());
			}
		),

		"<term>" => new Parsers\ConcatParser(
			array("TERM", "OPT-WHITESPACE"),
			function($term, $whitespace) {
				return $term;
			}
		),

		"TERM" => new Parsers\AlternativeParser(
			array(
				"LITERAL",
				"RULE-NAME"
			)
		),

		"LITERAL" => new Parsers\AlternativeParser(
			array(
				new Parsers\RegexParser('#^"([^"]*)"#', function($match0, $match1) { return $match1; }),
				new Parsers\RegexParser("#^'([^']*)'#", function($match0, $match1) { return $match1; })
			),
			function($text) {
				if($text == "") {
					return new Parsers\EmptyParser(function() { return ""; });
				}
				return new Parsers\StringParser($text);
			}
		),

		"RULE-NAME" => new Parsers\RegexParser("#^<[A-Za-z\\-]*>#"),

		"OPT-WHITESPACE" => new Parsers\RegexParser("#^[\t ]*#"),

		"EOL" => new Parsers\AlternativeParser(
			array(
				new Parsers\StringParser("\r"),
				new Parsers\StringParser("\n")
			)
		)
	),
	function($syntax) {
		$parsers = array();
		foreach($syntax as $rule) {

			if(count($parsers) === 0) {
				$top = $rule["rule-name"];
			}

			$parsers[$rule["rule-name"]] = $rule["expression"];
		}

		if(count($parsers) === 0) {
			throw new \Exception("No rules.");
		}

		return new Grammar($top, $parsers);
	}
);

// if executing this file directly, run unit tests
if(__FILE__ !== $_SERVER["SCRIPT_FILENAME"]) {
	return;
}

// Full rule set
$string = "
		<postal-address> ::= <name-part> <street-address> <zip-part>
		<name-part>      ::= <personal-part> <name-part> | <personal-part> <last-name> <opt-jr-part> <EOL>
		<personal-part>  ::= <initial> \".\" | <first-name>
		<street-address> ::= <house-num> <street-name> <opt-apt-num> <EOL>
		<zip-part>       ::= <town-name> \",\" <state-code> <ZIP-code> <EOL>
		<opt-jr-part>    ::= \"Sr.\" | \"Jr.\" | <roman-numeral> | \"\"

		<last-name>     ::= 'MacLaurin '
		<EOL>           ::= '\n'
		<initial>       ::= 'b'
		<first-name>    ::= 'Steve '
		<house-num>     ::= '173 '
		<street-name>   ::= 'Acacia Avenue '
		<opt-apt-num>   ::= '7A'
		<town-name>     ::= 'Stevenage'
		<state-code>    ::= ' KY '
		<ZIP-code>      ::= '33445'
		<roman-numeral> ::= 'g'
	";

$start = microtime(true);
$grammar2 = $bnfGrammar->parse($string);
print("Parsing completed in ".(microtime(true)-$start)." seconds\n");

$start = microtime(true);
$grammar2->parse("Steve MacLaurin \n173 Acacia Avenue 7A\nStevenage, KY 33445\n");
print("Parsing completed in ".(microtime(true)-$start)." seconds\n");

$string = "
		<syntax>         ::= <rule> | <rule> <syntax>
		<rule>           ::= <opt-whitespace> \"<\" <rule-name> \">\" <opt-whitespace> \"::=\" <opt-whitespace> <expression> <line-end>
		<opt-whitespace> ::= \" \" <opt-whitespace> | \"\"
		<expression>     ::= <list> | <list> \"|\" <expression>
		<line-end>       ::= <opt-whitespace> <EOL> <line-end> | <opt-whitespace> <EOL>
		<list>           ::= <term> | <term> <opt-whitespace> <list>
		<term>           ::= <literal> | \"<\" <rule-name> \">\"
		<literal>        ::= '\"' <text> '\"' | \"'\" <text> \"'\"

		<rule-name>      ::= 'a'
		<EOL>            ::= '\n'
		<text>           ::= 'b'
	";

$start = microtime(true);
$grammar3 = $bnfGrammar->parse($string);
print("Parsing completed in ".(microtime(true)-$start)." seconds\n");

$start = microtime(true);
$grammar3->parse(" <a> ::= 'b' \n");
print("Parsing completed in ".(microtime(true)-$start)." seconds\n");