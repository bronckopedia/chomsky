<?php
/**
 * Created by PhpStorm.
 * User: heinemann
 * Date: 19.02.16
 * Time: 08:22
 */
namespace chomsky;

spl_autoload_register(function($class) {
	$srcDir = implode(DIRECTORY_SEPARATOR, array(dirname(__FILE__), '..', 'src'));
	$parts = explode('\\', str_replace(__NAMESPACE__ . '\\', '', $class));
	$filepath = $srcDir . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $parts) . '.php';

	require_once $filepath;
});